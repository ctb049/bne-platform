# PeerTube

* [Wireframe](https://www.figma.com/file/uv0kgKBODhCcuo7ZDJHyPe/%E6%89%8B%E6%A9%9F%E7%89%88)
* [Peertube SourceCode](https://github.com/Chocobozzz/PeerTube)
* [Puretube Architecture](https://docs.joinpeertube.org/contribute-architecture)
* [User Requirement](https://docs.google.com/document/d/1TKPyEsZscS3KI27SnLPpqldrqSXr3093CiAnTfHHCnc/edit)
* [Gitlab Project Repository](https://gitlab.com/chaokenyuan/bne-platform)
* [Java Bittorrent lib](https://github.com/atomashpolskiy/bt)
* [JWT Login](https://vertx.io/docs/vertx-auth-jwt/java/)

### Mail List

| Name | eMail | Job 
| ----------|-----------------------------|----------------------|
| Johnny    | ctb049@gmail.com            | Product Owner
| Peter     | chaokenyuan@gmail.com       | PM
| Saker     | iamsaker.s001@gmail.com     | Java Backend
| Henry     | towerlondon0120@gmail.com   | TypeScripts Frontend
| Simon     | a9028109@gmail.com          | Java Guest
| ----------|-----------------------------|----------------------|
    



