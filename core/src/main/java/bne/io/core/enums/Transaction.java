package bne.io.core.enums;

import lombok.Getter;

@Getter
public enum Transaction {

    PAYMENT("payment",""),
    WITHDRAW("withdraw","");

    Transaction(String type, String description){
        this.type=type;
        this.description = description;
    }
    private final String type;

    private final String description;
}
