package bne.io.core.enums;

import lombok.Getter;

@Getter
public enum DigitalCoin {


    USDC("USDC", "USD Coin"),
    USDT("USDT", "USD Tether"),
    ETH("ETH", "Ethereum");

    private final String type;
    private final String description;

    DigitalCoin(String type, String description) {
        this.type = type;
        this.description = description;
    }

}
