package bne.io.core.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class Response {

    @JsonProperty("error_code")
    private int errorCode;

    @JsonProperty("error")
    private String errorMessage;

    @JsonProperty("message")
    private String errorSubMessage;

    public Response(Exception e) {
        this.errorMessage = e.toString();
    }
}
