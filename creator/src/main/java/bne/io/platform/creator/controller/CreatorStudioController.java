package bne.io.platform.creator.controller;

import bne.io.platform.creator.dto.AddObjectRequest;
import bne.io.platform.creator.dto.AddObjectResponse;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Api("Creator Studio Controller")
@Validated
@Slf4j
@RestController
@RequestMapping("/api/videos")
public class CreatorStudioController {


    @GetMapping(value = "/creator-studio", produces = MediaType.APPLICATION_JSON_VALUE)
    public String getCreatorStudio() {
        return "Creator Studio";
    }

    @PostMapping(value = "/upload", produces = MediaType.APPLICATION_JSON_VALUE)
    @Transactional
    public AddObjectResponse getAddResponse(@Valid @RequestBody AddObjectRequest request) throws Exception {

        return new AddObjectResponse();
    }

    @PostMapping("/upload")
    public void upload(@RequestParam("video") MultipartFile video,
                       @RequestParam("thumbnail") MultipartFile thumbnail,
                       @RequestParam("preview") MultipartFile preview) throws IOException {
        video.transferTo(new File("D:/tmp/videos/" + video.getOriginalFilename()));
        thumbnail.transferTo(new File("D:/tmp/thumbnails/" + thumbnail.getOriginalFilename()));
        preview.transferTo(new File("D:/tmp/previews/" + preview.getOriginalFilename()));

        System.out.println("Success");
    }

    @PostMapping("/upload-resumable")
    public String initializeUpload(@RequestParam("thumbnail") MultipartFile thumbnail,
                                   @RequestParam("preview") MultipartFile preview) {
        String id = UUID.randomUUID().toString();
        // create a new upload task with the given id
        // save the thumbnail and preview to local file system
        return id;
    }

    @PostMapping("/upload-resumable/{id}")
    public void uploadResumable(@PathVariable("id") String id,
                                @RequestParam("video") MultipartFile video) {
        // retrieve the upload task with the given id
        // save the video to local file system using the upload task's information
    }


}
