package bne.io.platform.creator.service;

import bne.io.platform.creator.dto.AddObjectRequest;
import bne.io.platform.creator.dto.AddObjectResponse;

public interface CreatorStudioService {
    public AddObjectResponse add(AddObjectRequest request);
}
