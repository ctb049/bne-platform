package bne.io.platform.creator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CreatorStudioApplication {

    public static void main(String[] args) {
        SpringApplication.run(CreatorStudioApplication.class, args);
    }

}